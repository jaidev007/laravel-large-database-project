<?php

use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\SigninController;
use Illuminate\Support\Facades\Auth;

// use App\Http\Controllers\JWTUserController;
// Route::group([
//     'middleware' => 'api',
//     'prefix' => 'auth'
// ], 
// function(){
//     // Route::post('login', [JWTUserController::class,'login']);
//     Route::post('test', [JWTUserController::class,'test']);
//     // Route::post('/logout', 'JWTUserController@logout');
//     // Route::post('/refresh', 'JWTUserController@refresh');
//     // Route::post('/me', 'JWTUserController@me');
// });

    Route::prefix('/pages')->group(function () {
        // Route::get('/home', function(){
        //     return view('about');
        // })->middleware('home_access');
        
        // Route::get('/about-us', function(){
        //     return view('about');
        // })->name('about')->middleware('home_access');

        Route::get('/', [UserController::class,'home']);
        Route::get('/home', [UserController::class,'home'])->name('home');
        Route::get('/about-us', [UserController::class,'about'])->name('about');
    });


// user
    Route::prefix('users')->group(function(){
        Route::get('/allusers',[UserController::class,'index'])->name('users');
        Route::get('/sign-up',[UserController::class,'create'])->name('sign-up');
        Route::post('/save',[UserController::class,'store'])->name('save_user_signup_data');
        Route::get('/edit/{id}',[UserController::class,'edit'])->name('update');
        Route::put('/update/{id}',[UserController::class,'update'])->name('update_user_signup_data');
        Route::get('/delete/{id}',[UserController::class,'destroy'])->name('delete');
    });

// login
    Route::prefix('users')->group(function(){
        Route::get('/signin',[SigninController::class,'signin'])->name('login')->middleware('guest');
        Route::post('/process',[SigninController::class,'process'])->name('process_user_signin');
        Route::get('/dashboard',[SigninController::class,'dashboard'])->name('dashboard')->middleware('auth');
        Route::get('/logout',[SigninController::class,'logout'])->name('logout')->middleware(['auth', 'preventCache']);
    });

//products
    Route::resource('products',ProductController::class);