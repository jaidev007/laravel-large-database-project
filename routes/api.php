<?php

use App\Http\Controllers\JWTUserController;
use GuzzleHttp\Middleware;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Illuminate\Routing\Router;

Route::group([
        'middleware' => 'api',
        'prefix' => 'auth'
    ], 
    function(Router $router){
        Route::post('login', [JWTUserController::class,'login'])->withoutMiddleware('auth:api');
        Route::post('test', [JWTUserController::class,'test']);
        Route::post('logout', 'JWTUserController@logout');
        Route::post('refresh', 'JWTUserController@refresh');
        Route::post('me', 'JWTUserController@me');
});