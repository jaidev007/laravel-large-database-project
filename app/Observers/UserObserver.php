<?php

namespace App\Observers;

use App\Models\User;
use App\Mail\UserRegistered;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class UserObserver
{
    /**
     * Handle the User "created" event.
     */
    public function created(User $user): void
    {
        \Log::channel('custom_log')->info('UserObserver/created | created observer triggered | new record created successfully');
        // $fullName = $user->firstname." ".$user->lastname;
        // Mail::to($user->email)->send(new UserRegistered($fullName));
        // WelcomeEmailNotification::send('Html.view', $data);
    }

    /**
     * Handle the User "updated" event.
     */
    public function updated(User $user): void
    {
        //
    }

    /**
     * Handle the User "deleted" event.
     */
    public function deleted(User $user): void
    {
        //
    }

    /**
     * Handle the User "restored" event.
     */
    public function restored(User $user): void
    {
        //
    }

    /**
     * Handle the User "force deleted" event.
     */
    public function forceDeleted(User $user): void
    {
        //
    }
}
