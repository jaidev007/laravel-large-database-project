<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class BlockPostRequests
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        \Log::channel('custom_log')->info('URL >> '. $request->url().', isMethod post >> '. $request->isMethod('post'));
        echo '<script>console.log("Direct access is denied")</script>';

        if($request->isMethod('post')) {
            echo '<script>console.log("Direct access is denied :(")</script>';
            echo '<script>alert("Direct access is denied :(")</script>';
            \Log::channel('custom_log')->info('Middleware Accessed By >> '. $request->url());
            return response()->json(['error' => 'Access denied for POST requests'], 403);
        }
        // If the request method is not Get, proceed with the request
        return $next($request);
    }
}
