<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class Aboutaccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        try{

            echo '<script>alert("welcome to about us. I am a middleware");</script>';
            echo '<script>console.log("welcome to about us. I am a middleware");</script>';
            \Log::channel('custom_log')->info('Middleware Accessed By >> '. $request->url());
            return $next($request);
        }
        catch(\Exception $e){
            \Log::channel('custom_log')->info('Aboutaccess/handle | Error >> '.$e->getMessage());            
        }
    }
}
