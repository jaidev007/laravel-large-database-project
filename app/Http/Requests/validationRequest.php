<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Log;

class validationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        // dd($this->all());
        $userID = $this->route('id');
        $userArr = $this->all();
        // $uniqueRule = Rule::unique('users', 'email')->ignore($userID ? $userID : null);

        return [
                'firstname' => 'required|string|max:25',
                'lastname' => 'required|string|max:25',
                'email' => 'required|email|max:150|regex:/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/|unique:users',
                'password' => 'required|string|min:8|max:16|confirmed',
                'password_confirmation' => 'required|string|min:8|max:16',
                'captcha' => 'required|captcha'
            ];
        // return [
        //     'firstname' => [
        //         'required' => 'While updating, the first name field is required.',
        //         'string' => 'The first name must be a string.',
        //         'max:25' => 'The first name may not be greater than :max characters.',
        //     ],
        //     'lastname' => [
        //         'required' => 'While updating, the last name field is required.',
        //         'string' => 'The last name must be a string.',
        //         'max:25' => 'The last name may not be greater than :max characters.',
        //     ],
        //     'email' => [
        //         'required' => 'The email field is required.',
        //         'max:150' => 'The email may not be greater than :max characters.',
        //         'regex:/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/' => 'The email format is invalid.',
        //     ],
        //     // 'email' => [
        //     //     'required' => 'The email field is required.',
        //     //     'max:150' => 'The email may not be greater than :max characters.',
        //     //     'regex:/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/' => 'The email format is invalid.',
        //     //     $uniqueRule => 'The email has already been taken.',
        //     //     function ($attribute, $value, $fail) use ($userArr) {
        //     //         if ($value !== $userArr['email'] && \App\Models\User::where('email', $value)->exists()) {
        //     //             // Check if email has been changed and if it already exists in the database
        //     //             $fail('The email has already been taken.');
        //     //         }
        //     //     },
        //     // ],
        //     'password' => [
        //         'required' => 'The password field is required.',
        //         'string' => 'The password must be a string.',
        //         'min:8' => 'The password must be at least :min characters.',
        //         'max:16' => 'The password may not be greater than :max characters.',
        //         'confirmed' => 'The password confirmation does not match.',
        //     ],
        //     'password_confirmation' => [
        //         'required' => 'While updating, the password confirmation field is required.',
        //     ],
        // ];
    }

    public function messages(){
        return [
            'password.confirmed' => 'The password confirmation does not match.',
            'firstname.required' => ':attribute required while update.',
            'lastname.required' => ':attribute required while update.',
            'email.required' => ':attribute required while update.',
            'password.required' => 'valid password required while update.',
            'captcha.captcha' => 'Passed captcha is not valid',
            'captcha.required' => 'The valid captcha string is required'
        ];
    }

    public function attributes(){
        return [
            'firstname' => 'Valid First Name',
            'lastname' => 'Valid Last Name',
            'email' => 'Valid Email',
            'password' => 'Valid Email',
        ];
    }
    
    protected function prepareForValidation():void
    {
        $this->merge([
            'firstname' => ucfirst($this->firstname),
            'lastname' => ucfirst($this->lastname)
        ]);
    }

}