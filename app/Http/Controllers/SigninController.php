<?php

namespace App\Http\Controllers;

use App\Models\User;
use Hamcrest\Type\IsNumeric;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class SigninController extends Controller
{
    public function signin(){
        $data['page_title'] = "Larde Data App | Sign In";
        return view('auth.signin',$data);
    }

    private function validation($req){
        return $req->validate([
            'email' => 'required|email|max:150|regex:/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/',
            'password' => 'required|string',
        ],[
            'email.required'=>'Valid email is required for signin',
            'password.required'=>'Valid password is required for signin'
        ]);
    }

    public function process(Request $req){
        $validatedData = $this->validation($req);

        try{
            $user = User::where('email', $validatedData['email'])->first();
            if($user && Hash::check($validatedData['password'], $user->password)){
                Auth::login($user);
                return redirect()->route('dashboard');
            }
            else{
                return redirect()->back()->with('error','Login Failed, Please check your credentials :(');
            }
        }
        catch(\Exception $e){
            \Log::channel('custom_log')->info('SigninController/process | Error >> '.$e->getMessage());
            return redirect()->back()->with('error','Some technical error occurred :(');
        }
    }

    public function dashboard(){
        return view('dashboard');
    }

    public function logout(){
        Auth::logout();
        return redirect()->route('login');
    }

}