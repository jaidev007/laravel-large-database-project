<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use App\Http\Requests\validationRequest;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{

    public function home(){
        $data['page_title'] = 'Home | Large Data App';
        $data['heading'] = 'Welcome to Home';
        return view('about', $data);
    }

    public function about(){
        $data['page_title'] = 'About us | Large Data App';
        $data['heading'] = 'Welcome to About us';
        return view('about', $data);
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data['page_title'] = 'All Users | Large Data App';

        // ********** use them below to make dynamic sorting **********************

        // $sortField = $request->input('sort', 'created_at'); // Default sort field
        // $sortOrder = $request->input('order', 'desc'); // Default sort order
        // $perPage = $request->input('perPage', 10); // Number of items per page
        
        $data['user_data'] = User::orderBy('id','desc')->paginate(15,['id','name','email','created_at'],'pages');
        // Check if the data exists in the cache
        // if(Cache::has('user_data')) {
        //     // Retrieve the data from the cache
        //     $users = Cache::get('user_data');
        // }
        // else{
        //     // If data doesn't exist in the cache, fetch it from the database
        //     $users = User::orderBy('id', 'desc')->paginate(25);
            
        //     // Store the data in the cache for 1 hour (you can adjust the time as needed)
        //     Cache::put('user_data', $users, now()->addHour());
        // }

        return view('users',$data);
    }


    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $data['page_title'] = 'Sign Up | Large Data App';
        return view('signup',$data);
    }

    /**
     * Store a newly created resource in storage.
     */


    public function store(Request $request)
    {
        $validatedData = $this->validation($request);
        try{
            $userTable = new User();
            $userTable->name = $validatedData['firstname'].' '.$validatedData['lastname'];
            $userTable->email = $validatedData['email'];
            $userTable->password = Hash::make($validatedData['password']);
            $userTable->saveOrFail();
            
            // ** observer start here and send email and sms notification

            return redirect()->back()->with('success','Data received successfully :)');
        }
        catch(\Exception $e){
            \Log::channel('custom_log')->info('userController/store | Error >> '.$e->getMessage());
            return redirect()->back()->with('error','Some technical error occurred :(');
        }

        /*  
            -----------
              Pending
            -----------
            xxx--- indian time
            xxx--- middleware
            xxx-- adding and using dependencies
            xxx-- Log with custom file name
            login (sanctum vs passport vs jwt)
            OAuth2 with facebook, google
            save into cache 
            get data from cache (redis)
            RESTful APIs
            maximum hit limits
            multi language
            multi database connections & its advantages
            millions of records fetch optimized
            properly unit testing
        */
    }

    private function validation($req){
        return $req->validate([
            'firstname' => 'required|string|max:25',
            'lastname' => 'required|string|max:25',
            'email' => 'required|email|max:150|regex:/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/|unique:users',
            'password' => 'required|string|min:8|max:16|confirmed',
            'password_confirmation' => 'required|string|min:8|max:16',
            'captcha' => 'required|captcha'
        ], [
            'password.confirmed' => 'The password confirmation does not match.',
            'captcha.captcha' => 'Passed captcha is not valid',
            'captcha.required' => 'The valid captcha string is required'
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $data['page_title'] = 'Update User | Large Data App';

        try{
            $data['user'] = User::findOrFail($id);
        }
        catch(\Exception $e){
            // $data['user_data'] = null;
            \Log::channel('custom_log')->info('userController/edit | Error >> '.$e->getMessage());
            session()->flash('error', 'No record found :(');
        }

        return view('userupdate',$data);
    }


    /**
     * Update the specified resource in storage.
     */
    public function update(validationRequest $request, string $id)
    {
        $validatedData =  $request->all(); 
        // dd($id);

        try{
            $user = User::findOrFail($id)->update([
                'name' => $validatedData['firstname'].' '.$validatedData['lastname'],
                'email' => $validatedData['email'],
                'password' => Hash::make($validatedData['password']),
                'updated_at' => now(),
            ]);
            return redirect()->back()->with('success','Data updated successfully :)');

        }
        catch(\Exception $e){
            \Log::channel('custom_log')->info('userController/update | Error >> '.$e->getMessage());
            return redirect()->back()->with('error','Some technical error occurred :(');
        }
    }


    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        try {
            $user = User::find($id);
            $user->delete();
            return redirect()->back()->with('success', 'Record deleted successfully');
        }
        catch (\Exception $e) {
            \Log::channel('custom_log')->info('userController/destroy | Error >> '.$e->getMessage());
        }
    }
}
