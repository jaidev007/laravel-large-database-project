<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Faker\Factory as FakeData;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $faker = FakeData::Create();

        //generate fake 5000 records

            // 'name'=>Str::random(15),
            // 'email'=>Str::random(10) . '@gmail.com',
            // 'password'=> Hash::make('password@4321')

        for($i=0;$i<5000;$i++){
            try {
                $email = $faker->unique()->safeEmail;
                DB::table('users')->insert([
                    'name' => $faker->name,
                    'email' => $email,
                    'password' => Hash::make('1234@User;')
                ]);
                $faker->unique(false); // Disable unique rule temporarily to allow generating more unique emails
            }
            catch(\Exception $e){
                continue;
            }
        }

    }
}
