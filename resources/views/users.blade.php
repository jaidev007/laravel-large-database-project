@extends('common.userheader')

    @section('navbar')
                
        <ul class="font-medium flex flex-col p-4 md:p-0 mt-4 border border-gray-100 rounded-lg bg-gray-50 md:flex-row md:space-x-8 rtl:space-x-reverse md:mt-0 md:border-0 md:bg-white dark:bg-gray-800 md:dark:bg-gray-900 dark:border-gray-700">
            <li>
            <a href="{{ route('home') }}" class="block py-2 px-3 text-white bg-blue-700 rounded md:bg-transparent md:text-blue-700 md:p-0 dark:text-white" aria-current="page">Home</a>
            </li>
            <li>
            <a href="{{ route('about') }}" class="block py-2 px-3 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-white md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent">About</a>
            </li>
            <li>
            <a href="{{ route('sign-up') }}" class="block py-2 px-3 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-white md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent">Signup</a>
            </li>
            <li>
            <a href="{{ route('login') }}" class="block py-2 px-3 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-white md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent">Signin</a>
            </li>
            <li>
            <a href="" class="block py-2 px-3 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-white md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent md:dark:text-blue-500">All Users</a>
            </li>
        </ul>

    @endsection


    @section('pagecontent')
        <style>
            table,.pagination{width:80%;text-align:left;margin:10px auto;}
            th,.actions{text-align:center;font-size:0.75rem;}
            th,td{border:1px solid #393737;padding:0.45rem 0.45rem;}
            .actions a{margin:0 0.75rem;}
        </style>
        @if(session()->has('success'))
            <div class="p-4 text-sm text-green-800 rounded-lg bg-green-50 dark:bg-green-100 dark:text-green-800 text-center">{{ session()->get('success') }}</div>
        @endif
        <table class="table-auto rounded-lg dark:bg-gray-300 md:dark:bg-gray-300">
        <thead class="dark:bg-pink-400 md:dark:bg-purple-400">
            <tr>
            <th>ID</th>
            <th>Full Name</th>
            <th>Email</th>
            <th>Created At</th>
            <th>Action</th>
            </tr>
        </thead>
        <tbody>
                @forelse($user_data as $record)
                    <tr>
                        <td>{{ $record->id }}</td>
                        <td>{{ $record->name }}</td>
                        <td>{{ $record->email }}</td>
                        <td>{{ $record->created_at }}</td>
                        <td class="actions">
                            <a href="{{ route('update', ['id' => $record->id]) }}" class="my-1 py-1 px-2 bg-violet-500 text-white font-normal rounded-md shadow-md hover:bg-violet-700 focus:outline-none focus:ring focus:ring-violet-400 focus:ring-opacity-75">Edit</button>
                            <a href="{{ route('delete', ['id' => $record->id]) }}" class="my-1 py-1 px-2 bg-red-500 text-white font-normal rounded-md shadow-md hover:bg-red-700 focus:outline-none focus:ring focus:ring-red-400 focus:ring-opacity-75">Delete</button>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="5">No Record Found :(</td>
                    </tr>
                @endforelse
        </tbody>
        </table>

        <div class="pagination">
            {{ $user_data->links('pagination::tailwind') }}
        </div>        
    @endsection 