@extends('common.userheader')

    @section('navbar')
        
        <ul class="font-medium flex flex-col p-4 md:p-0 mt-4 border border-gray-100 rounded-lg bg-gray-50 md:flex-row md:space-x-8 rtl:space-x-reverse md:mt-0 md:border-0 md:bg-white dark:bg-gray-800 md:dark:bg-gray-900 dark:border-gray-700">
            <li>
            <a href="{{ route('home') }}" class="block py-2 px-3 text-white bg-blue-700 rounded md:bg-transparent md:text-blue-700 md:p-0 dark:text-white" aria-current="page">Home</a>
            </li>
            <li>
            <a href="{{ route('about') }}" class="block py-2 px-3 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-white md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent">About</a>
            </li>
            <li>
            <a href="{{ route('sign-up') }}" class="block py-2 px-3 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-white md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent">Signup</a>
            </li>
            <li>
            <a href="" class="block py-2 px-3 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-white md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent md:dark:text-blue-500">Signin</a>
            </li>
            <li>
            <a href="{{ route('users') }}" class="block py-2 px-3 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-white md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent">All Users</a>
            </li>
        </ul>

    @endsection

    @section('signinForm')
        @if(session()->has('success'))
            {{ session()->get('success') }}
        @endif
        <form action="{{route('store-user')}}" method="post">
            @csrf
            <label for="name">Name *</label>
            <input type="text" name="name" id="name" value="{{old('name')}}" />
            <br/><br/>
            <label for="email">Email *</label>
            <input type="email" name="email" id="email" value="{{old('email')}}" />
            <br/><br/>
            <label for="password">Password *</label>
            <input type="password" name="password" id="password" value="{{old('password')}}" />
            <br/><br/>
            <input type="submit" name="save" id="save" value=" SAVE >> " />
        </form>
    @endsection